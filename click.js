const chrome = require('selenium-webdriver/chrome');
const {Builder, By, Key, until} = require('selenium-webdriver');

const screen = {
    width: 640,
    height: 480
};

(async function example() {

    let driver;

    //Real browser mode
    driver = await new Builder().forBrowser('chrome').build();

    //Headless browser mode
    //driver = await new Builder().forBrowser('chrome').setChromeOptions(new chrome.Options().headless().windowSize(screen)).build();


    try {
        console.log("0");
        await driver.get('https://www.arealme.com/spacebar-test/en/');
        console.log("1");


        const title = await driver.getTitle();
        console.log(title);

        //await driver.wait(until.titleIs('webdriver - Google Search'), 5000);
        let button = await driver.wait(until.elementLocated(By.id('start')), 10000);

        await button.click();

        let body = driver.findElement(By.tagName('body'));

        for (let i = 0; i < 300; i++) {
            body.sendKeys(Key.SPACE);
        }

        body.sendKeys(Key.SPACE);


        console.log("2");

    } catch {

        console.log("TITLE NO FOUND");

    } finally {
        //await driver.quit();
    }
})();