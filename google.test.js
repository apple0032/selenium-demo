const chrome = require('selenium-webdriver/chrome');
const {Builder, By, Key, until} = require('selenium-webdriver');

const screen = {
    width: 640,
    height: 480
};

(async function example() {

    let driver;

    //Real browser mode
    //driver = await new Builder().forBrowser('chrome').build();

    //Headless browser mode
    driver = await new Builder().forBrowser('chrome').setChromeOptions(new chrome.Options().headless().windowSize(screen)).build();


    try {
        console.log("0");
        await driver.get('http://www.google.com/ncr');
        console.log("1");
        await driver.findElement(By.name('q')).sendKeys('webdriver', Key.RETURN);
        console.log("2");

        const title = await driver.getTitle();
        console.log(title);

        await driver.wait(until.titleIs('webdriver - Google Search'), 10000);
        console.log("3");

    } catch {

        console.log("TITLE NO FOUND");

    } finally {
        await driver.quit();
    }
})();