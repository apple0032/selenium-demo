const chrome = require('selenium-webdriver/chrome');
const {Builder, By, Key, until} = require('selenium-webdriver');
const fs = require('fs');
const https = require('https');

const screen = {
    width: 640,
    height: 480
};

(async function example() {

    let driver;

    //Real browser mode
    //driver = await new Builder().forBrowser('chrome').build();

    //Headless browser mode
    driver = await new Builder().forBrowser('chrome').setChromeOptions(new chrome.Options().headless().windowSize(screen)).build();

    let link = [];

    try {
        console.log("----0----");
        await driver.get('file:///C:/Users/Ken%20Ip/Desktop/test-selenium/dummy.html');
        console.log("----1----");

        const title = await driver.getTitle();
        console.log(title);

        var content_area = await driver.wait(until.elementLocated(By.id('content_area')), 10000);
        console.log("----2----");

        var pagename = await driver.wait(until.elementLocated(By.id('div_pagename')), 10000).getText();
        console.log(pagename);
        console.log("----3----");

        var lastRevision = await driver.wait(until.elementLocated(By.id('div_revisiondate')), 10000).getText();
        console.log(lastRevision);
        console.log("----4----");

        //let lists = await driver.findElement(By.xpath('//*[@id="sizecontrol"]/table/tbody/tr[3]/td/table/tbody/tr/td[2]/table[4]/tbody/tr/td[2]/div/ul/li[2]/ul')).getText();
        //console.log(lists);

        // let department = await driver
        //     .findElement(By.xpath('//*[@id="sizecontrol"]/table/tbody/tr[3]/td/table/tbody/tr/td[2]/table[4]/tbody/tr/td[2]/div/ul/li[2]/ul/li[1]'));
        // let tag = await department.findElement(By.tagName('a')).getAttribute("href");
        // console.log(tag);

        // let department2 = await driver
        //     .findElement(By.xpath('//*[@id="sizecontrol"]/table/tbody/tr[3]/td/table/tbody/tr/td[2]/table[4]/tbody/tr/td[2]/div/ul/li[2]/ul/li[2]'));
        // let tag2 = await department2.findElement(By.tagName('a')).getAttribute("href");
        // console.log(tag2);

        /***** Website version *****/
        //let lists = await driver.findElement(By.xpath('//*[@id="sizecontrol"]/table/tbody/tr[3]/td/table/tbody/tr/td[2]/table[4]/tbody/tr/td[2]/div/ul/li[2]/ul'));

        /***** Text only version *****/
        let lists = await driver.findElement(By.xpath('/html/body/div[2]/ul/li[2]/ul'));

        let items = await lists.findElements(By.tagName('li'));

        let count = 0;
        for (item of items) {
            let pdf     = await item.findElement(By.tagName('a')).getAttribute("href");
            let name    = await item.getText();

            link.push(pdf);
            console.log(pdf);
            downloadPdf(pdf,name,count);
            count++;
        }



        /***********
         * Workflow

         1. Get lastRevision date value
         2. Compare lastRevision value with the value from database
         3. Pseudo logic

         RUN CRON JOB WITHIN A PERIOD {
             if( lastRevision value != database value ) {
                A. Crawl all pdfs from webpage
                B. Compare with the pdfs from the "old" folder (stored all old version pdf)
                C. Send email if there is a difference between the pdfs
                D. Replace all old pdfs from "old" folder by the newest pdfs
                E. Set database value = current lastRevision value
             } else {
                //Assume PDFs no update in the page, do nothing.
             }
         }

         ***********/



    } catch(e) {

        console.log("!!!!!!!!!!SOMETHING ERROR!!!!!!!!!!");
        console.log(e);

    } finally {
        await driver.quit();
    }

})();


function downloadPdf(url , name , count){

    let filename = name.replace("[PDF]", "").trim();

    https.get(url,(res) => {
        const path = `${__dirname}/pdf/${filename}.pdf`;
        const filePath = fs.createWriteStream(path);
        res.pipe(filePath);
        filePath.on('finish',() => {
            filePath.close();
            console.log('Download Completed');
        })
    })
}